# native imports
import pickle
import tkinter as tk

# external imports

# custom imports
from toolbar import ToolBarFrame
from tileframe import TileFrame

class Main(tk.Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.geometry("1280x600")
        self.title("Pygame Level Creator")
        self.font = ("System", 16, "bold")

        self.LEVEL_NAME = None

        self.TILE_MODE = "floor"
        self.TILESET_NAME = None
        self.TILESET = None

        self.GR_TILESET_NAME = None

        self.MAP_WIDTH = None
        self.MAP_HEIGHT = None

        self.TILESET_WIDTH = None
        self.TILESET_HEIGHT = None

        # Widget Definitions

        self.toolbar = ToolBarFrame(self, bd=1, relief="raised")
        self.tileframe = TileFrame(self)

        # For shifting between all the tiles in a tileset
        self.bind("<Left>", lambda e: self.toolbar.shiftTiles("LEFT"))
        self.bind("<Right>", lambda e: self.toolbar.shiftTiles("RIGHT"))

        # For changing between the different modes to give tiles properties
        self.bind("1", lambda e: self.setMode(0))
        self.bind("2", lambda e: self.setMode(1))
        self.bind("3", lambda e: self.setMode(2))
        self.bind("4", lambda e: self.setMode(3))

        # Widget Placement

        self.toolbar.pack(side="top", fill="x")
        self.tileframe.pack(side="top", fill="both", expand=True)

        self.SELECTED_TILE = self.toolbar.selected_tile
    
    def exportLevel(self):
        level = {
            "tileset":self.TILESET_NAME,
            "ground":self.GR_TILESET_NAME,
            "width":self.TILESET_WIDTH,
            "height":self.TILESET_WIDTH,
            "map_width":self.MAP_WIDTH,
            "map_height":self.MAP_HEIGHT,
            "tiles": [],
        }

        for tile in self.tileframe.tiles:
            data = (
                tile.getX(), tile.getY(),
                tile.getWall(), tile.getOverlay()
            )
            if None in data:
                data = None

            level["tiles"].append(data)
            
        with open(self.LEVEL_NAME+".level", "wb") as level_file:
            pickle.dump(level, level_file, protocol=pickle.DEFAULT_PROTOCOL)

    # For the mode
    def setMode(self, mnum):
        modes = ("floor","wall","overlay","ground")
        colors = ("black", "red", "blue","green")
        self.TILE_MODE = modes[mnum]
        self.toolbar.mode_label.configure(text="MODE: "+self.TILE_MODE, fg=colors[mnum])

        if mnum == 0:
            self.tileframe.toggleWallTiles("hidden")
            self.tileframe.toggleOverlayTiles("hidden")
            self.tileframe.toggleGroundTiles("hidden")
        elif mnum == 1:
            self.tileframe.toggleOverlayTiles("hidden")
            self.tileframe.toggleGroundTiles("hidden")
            self.tileframe.toggleWallTiles("normal")
        elif mnum == 2:
            self.tileframe.toggleWallTiles("hidden")
            self.tileframe.toggleGroundTiles("hidden")
            self.tileframe.toggleOverlayTiles("normal")
        else:
            self.tileframe.toggleWallTiles("hidden")
            self.tileframe.toggleOverlayTiles("hidden")
            self.tileframe.toggleGroundTiles("normal")

    def getMode(self):
        return self.TILE_MODE

    # For the tile set
    def setTileSet(self, value):
        self.TILESET = value
    
    def getTileSet(self):
        return self.TILESET

    def setTileSetName(self, value):
        self.TILESET_NAME = value
    
    def getTileSetName(self):
        return self.TILESET_NAME

    def setTileSetWidth(self, value):
        self.TILESET_WIDTH = value
    
    def setTileSetHeight(self, value):
        self.TILESET_HEIGHT = value

    def getTileSetWidth(self):
        return self.TILESET_WIDTH
    
    def getTileSetHeight(self):
        return self.TILESET_HEIGHT

    # For the ground tileset

    def setGround(self, value):
        self.GR_TILESET_NAME = value

    def getGround(self):
        return self.GR_TILESET_NAME

    # For the map
    def setMapWidth(self, value):
        self.MAP_WIDTH = value

    def setMapHeight(self, value):
        self.MAP_HEIGHT = value
    
    def getMapWidth(self):
        return self.MAP_WIDTH
    
    def getMapHeight(self):
        return self.MAP_HEIGHT
    
    # For the level name
    def setLevelName(self, value):
        self.LEVEL_NAME = value

    # Other
    def getSelectedTile(self):
        x = self.SELECTED_TILE.getX()
        y = self.SELECTED_TILE.getY()
        image = self.SELECTED_TILE.getImage()
        return (x, y, image)
    
    def createMap(self):
        self.tileframe.createMap()
    
    def drawGround(self):
        if self.GR_TILESET_NAME is not None:
            self.tileframe.drawGround()

if __name__ == "__main__":
    root = Main()
    root.mainloop()